$( document ).ready(function() {
    //define the scrollable

    let scrollableElement = document.body;

    scrollableElement.addEventListener('wheel', function (){
        //define the element that will be taken as reference for the computation.
        let element = document.getElementById('content')
        //compute the ratio between the offset from the top of the window to the #content div and the window' height
        let ratio = (Math.round(element.getBoundingClientRect().top)/window.innerHeight)
        //avoid logoOpacity to backtrack
        if (ratio<0){
            ratio = 0
        }
        //compute opacity of Logo depending on the position of the div #content and on the window's height
        let logoOpacity = 1 + Math.log10(ratio**4);

        //change the opacity of logos.
        $("#logoGala").css('opacity', logoOpacity)
        $('header').css('background-color', 'rgba(3,1,40    ,'+ -logoOpacity +')')
        $("#logoCorner").css('opacity', -logoOpacity)

        })
});