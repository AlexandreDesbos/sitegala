function loadAnimation(divId, loop, autoplay){

    bodymovin.loadAnimation({
        crossOrigin: null,
        container: document.getElementById(divId),
        renderer: 'svg',
        loop: loop,
        autoplay: autoplay,
        path: 'assets/'+divId+'.json'
    })

}
