
function goToDiv(divId, speed) {
    // Scroll
    let logoOpacity = document.getElementById('logoGala').style.opacity

    if ((parseInt(logoOpacity, 10) > 0 || !logoOpacity)  && divId !== '#landingPage'){
        document.getElementById('logoGala').style.opacity = '0';
        document.getElementById('logoCorner').style.opacity = '1';
        $('header').css('background-color', 'rgba(1,0,25,1)')

    }

    else if (parseInt(logoOpacity, 10) <= 0 && divId === '#landingPage'){
        setTimeout(function(){
            document.getElementById('logoGala').style.opacity = '1';
            document.getElementById('logoCorner').style.opacity = '0';
            $('header').css('background-color', 'rgba(1,0,25,0)')
        }, 400);


    }

    $('html,body').animate({
        scrollTop:
        $(divId).offset().top - 80

    }, speed);


}

