$(document).ready(function () {
    $("#mobile-menu-open").click(function () {
        $('#mobile-menu-open').hide();
        $('#logoGala').hide();
        $('header').css('height', '40vh')
            .css('background-color', 'rgba(3,1,40,1)')
        $('nav').show()
    });

    $("section").click(function () {
        if ($(window).width() < 1050){

            $('#mobile-menu-open').show();
            $('nav').hide()
            $('header').css('height', '10vh')
            $('#logoGala').show();

        }

    });

});